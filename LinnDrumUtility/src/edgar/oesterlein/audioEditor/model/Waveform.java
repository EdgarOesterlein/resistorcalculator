package edgar.oesterlein.audioEditor.model;

public enum Waveform {
	SINE("Sinus"), SQUARE("Rechteck"), SAWTOOTH("S�gezahn"), TRIANGLE("Dreieck");
	private final String display;
	private Waveform(String s) {
		display = s;
	}
	
	@Override
	public String toString() {
		return display;
	}
}
