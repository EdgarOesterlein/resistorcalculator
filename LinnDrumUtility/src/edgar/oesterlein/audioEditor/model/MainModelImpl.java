package edgar.oesterlein.audioEditor.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.TrackListListener;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;


public class MainModelImpl implements MainModel {
	private File settingsFile, currentWorkingDirectory;
	private Settings settings;
	
	public Settings getSettings() {
		return settings;
	}
	
	
	private List<Track> trackList = new ArrayList<>(TRACKLISTSIZE_INIT);
	private List<TrackListListener> trackListListener = new ArrayList<>(TRACKLISTSIZE_INIT);
	
	public void addTrackListListener(TrackListListener l) {
		trackListListener.add(l);
	}

	public void removeTrackListListener(TrackListListener l) {
		trackListListener.remove(l);
	}
	
	private void notifyTrackAdded(Track track) {
		for(TrackListListener listener : trackListListener) {
			listener.trackAdded(track);
		}
	}
	
	private void notifyTrackRemoved(Track track_ref) {
		for(TrackListListener listener : trackListListener) {
			listener.trackRemoved(track_ref);
		}
	}

	@Override
	public Track addTrack(Track track) {
		deselectAllTracks();
		trackList.add(track);
		notifyTrackAdded(track);
		return track;
	}


	//Creates an empty mono track, deselects all other tracks and marks this as selected.
	@Override
	public MonoTrack addEmptyMonoTrack() {
		MonoTrack monoTrack = new MonoTrack(settings.getStandard_sampleRate(), settings.getStandard_bitRate());
		monoTrack.setName("MonoTrack_" + tracksCreatedCount());
		deselectAllTracks();
		monoTrack.setSelected(true);
		trackList.add(monoTrack);
		notifyTrackAdded(monoTrack);
		return monoTrack;
	}

	@Override
	public StereoTrack addEmptyStereoTrack() {
		StereoTrack stereoTrack = new StereoTrack(settings.getStandard_sampleRate(), settings.getStandard_bitRate());
		stereoTrack.setName("StereoTrack_" + tracksCreatedCount());
		deselectAllTracks();
		stereoTrack.setSelected(true);
		trackList.add(stereoTrack);
		notifyTrackAdded(stereoTrack);
		return stereoTrack;
	}

	@Override
	public Track copyTrack(Track createdTrack) {
		if(createdTrack.getTrackChannelType() == TrackChannelType.MONO)
			trackList.add(new MonoTrack(createdTrack.toMonoTrack()));
		else if(createdTrack.getTrackChannelType() == TrackChannelType.STEREO)
			trackList.add(new StereoTrack(createdTrack.toStereoTrack()));
		notifyTrackAdded(createdTrack);
		return createdTrack;	
	}


	public void removeTrack(Track track_ref) {
		notifyTrackRemoved(track_ref);
		trackList.remove(track_ref);
	}


	public void deselectAllTracks() {
		for(Track track : trackList)
			if(track.isSelected())
				track.setSelected(false);
	}

	public MainModelImpl() {
		//Locate the current working directory for the settings-file.
		settingsFile = new File("settings.dat");
		currentWorkingDirectory = new File(new File(".").getAbsolutePath());
	}
	
	public int getSelectedTracksCount() {
		int count = 0;
		for(Track track : trackList)
			if(track.isSelected())
				count++;
		return count;
	}

	//Es wird immer eine frische Liste erzeugt mit den zurzeit markierten Tracks.
	public List<Track> getSelectedTracks() {
		List<Track> selectedTracks = new ArrayList<>(trackList.size());
		for(Track track : trackList)
			if(track.isSelected())
				selectedTracks.add(track);
		return selectedTracks;
	}
	
	
	public int tracksCreatedCount() {
		return trackList.size();
	}
	
	public boolean doesSettingsFileExist() throws IOException {
		return FileUtils.directoryContains(currentWorkingDirectory, settingsFile);
	}
	
	//TODO: XML-Serialization
	public void loadSettings() throws IOException, ClassNotFoundException {
		if(doesSettingsFileExist()) {
			//Read the settings file
			ObjectInputStream oistream = new ObjectInputStream(new FileInputStream(settingsFile));
			settings = (Settings)oistream.readObject();
			oistream.close();
		}
		else { //If not existent create and initialize the new settings object 
			settings = Settings.getInstance();
		}
	}
	
	//TODO: XML-Serialization
	public void saveSettings() throws IOException {
		File settingsPath = new File(currentWorkingDirectory, settingsFile.getName());
		FileOutputStream fos = new FileOutputStream(settingsPath);
		
		ObjectOutputStream oostream = new ObjectOutputStream(fos);
		oostream.writeObject(settings);
		oostream.close();
	}
}
