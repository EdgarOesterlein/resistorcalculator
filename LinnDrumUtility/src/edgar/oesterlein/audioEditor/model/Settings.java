package edgar.oesterlein.audioEditor.model;

import java.io.Serializable;

public class Settings implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	//Created as a Singleton.
	private static Settings instance;
	
	public static Settings getInstance() {
		if(instance == null)
			instance = new Settings();
		return instance;
	}
	
	private Settings() {
		//Initialization
		setStandard_sampleRate(44100);
		setStandard_bitRate(32);
	}
	
	//Attributes
	private int standard_sampleRate, standard_bitRate;

	public int getStandard_sampleRate() {
		return standard_sampleRate;
	}

	public void setStandard_sampleRate(int standard_sampleRate) {
		this.standard_sampleRate = standard_sampleRate;
	}

	public int getStandard_bitRate() {
		return standard_bitRate;
	}

	public void setStandard_bitRate(int standard_bitRate) {
		this.standard_bitRate = standard_bitRate;
	}
	
	
}
