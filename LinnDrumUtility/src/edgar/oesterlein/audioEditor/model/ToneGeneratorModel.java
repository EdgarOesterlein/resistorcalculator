package edgar.oesterlein.audioEditor.model;

import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.DCA;
import edgar.oesterlein.audioEditor.audioProcessing.Oscillator;
import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;

public class ToneGeneratorModel {
	private List<Track> selectedTracks;
	
	private double frequency, amplitudeFactor, durationOfRecording;
	private Waveform waveform;
	
	public List<Track> getSelectedTracks() {
		return selectedTracks;
	}
	
	public void setWaveform(Waveform waveform) {
		this.waveform = waveform;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public void setAmplitudeFactor(double amplitudeFactor) {
		this.amplitudeFactor = amplitudeFactor;
	}

	public void setDurationOfRecording(double durationOfRecording) {
		this.durationOfRecording = durationOfRecording;
	}

	public ToneGeneratorModel(List<Track> selectedTracks) {
		this.selectedTracks = selectedTracks;
	}

	public void generate() {
		for(Track track : selectedTracks)
			generateToTrack(track);
	}
	
	private void generateToList(List<Integer> values_ref, int sampleRate) {
		values_ref.clear();
		double _time = 0;
		Oscillator oscillator = new Oscillator();
		oscillator.setFrequency(frequency);
		
		int value = 0;
		for(int i = 0; i < sampleRate * durationOfRecording; i++) {
			
			if(waveform.equals(Waveform.SINE))
				value = (int)oscillator.sine(_time);
			else if(waveform.equals(Waveform.SQUARE))
				value = (int)oscillator.square(_time);
			else if(waveform.equals(Waveform.SAWTOOTH))
				value = (int)oscillator.sawtooth(_time);	
			else if(waveform.equals(Waveform.TRIANGLE))
				value = (int)oscillator.triangle(_time);
			
			value = (int) DCA.process(value, amplitudeFactor);
			values_ref.add(value);	//TODO: Sauber runden!
			_time += 1f / sampleRate;
		}
	}
	
	//Takes a reference of a track and fills it with a waveform dependent on the given parameters.
	private void generateToTrack(Track track) {
		int sampleRate = track.getSampleRate();
		if(track.getTrackChannelType() == TrackChannelType.MONO) 
			generateToList(track.toMonoTrack().getValues(), sampleRate);
		else if(track.getTrackChannelType() == TrackChannelType.STEREO) {
			generateToList(track.toStereoTrack().getValuesLeftChannel(), sampleRate);
			generateToList(track.toStereoTrack().getValuesRightChannel(), sampleRate);
		}
	}
}
