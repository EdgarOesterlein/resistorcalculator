package edgar.oesterlein.audioEditor.model;

import edgar.oesterlein.audioEditor.audioProcessing.TrackListListener;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;

public interface MainModel {
	Track addTrack(Track track);
	MonoTrack addEmptyMonoTrack();
	StereoTrack addEmptyStereoTrack();
	void removeTrack(Track track_ref);
	Track copyTrack(Track createdTrack);
	void deselectAllTracks();
	
	void addTrackListListener(TrackListListener l);
	void removeTrackListListener(TrackListListener l);
	
	int TRACKLISTSIZE_INIT = 16; //TODO: irgendwas anderes einfallen lassen...
}
