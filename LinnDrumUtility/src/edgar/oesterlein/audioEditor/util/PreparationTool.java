package edgar.oesterlein.audioEditor.util;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

/*
 * This tool prepares the given text-file to a sequence of bytes which are 
 * saved to a binary-file with a specific output path.
 * */

public class PreparationTool {
	/* Loads and converts the unprepared text-file, in which the HEX-values are stored as a human-readable text, 
	 * and saves it as a raw binary file, just like an EPROM-Image.
	 * 
	 * The Linn 16-bit EPROM values are stored as 2x 8-bit HEX-Values. The first byte is the MSB of the first sample.
	 * The second byte is the LSB of the first sample, the third byte is the MSB of the second sample and so on.
	 * Here is an example of how a line in the text-file looks like. The line under the example shows the byte order: 
	 *  0x20,0x6A,0x20,0x7A,0x20,0xA2,0x20,0xCA,...
	 * ( MSB, LSB, MSB, LSB, MSB, LSB, MSB, LSB,...)
	 * 
	 * 
	 * They will look like this (without the ""): "0xFF, 0xAF, 0x7A, ..."
	 * Each value represents a byte.
	 * These string values are then converted to int-values for better handling and then they will be saved with 
	 * the call of another method. 
	 */
	public static void prepareTextualRepresentation(File inputTextFile, File outputBinary) throws IOException { 
		//At first every line of the given file is stored in a list...
		List<String> fileLines = FileUtils.readLines(inputTextFile);
		ArrayList<Integer> unpreparedValues = new ArrayList<Integer>(32*fileLines.size());
		//... and then every line is split up in its values.  
		for(String line : fileLines) {
			String[] rawHEXValues = line.split(",");
			//These values will be saved in a temporary list.
			for(String hexValue : rawHEXValues) {
				unpreparedValues.add(Integer.decode(hexValue.trim()));
			}	
		}
		saveBinaryFile(unpreparedValues, outputBinary);
	}
	
	/*
	 * Save the given values to a binary-file. The byte order is just the same as the textual representation of the given text-file.
	 * */
	private static void saveBinaryFile(ArrayList<Integer> byteValues, File fileOutput) throws IOException {
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(fileOutput));
		for(int value : byteValues)
			dos.writeByte(value);
		dos.close();
	}
	
	
	//Loads the binary EPROM-file and returns the values as integers. Each integer-value contains the MSB and the LSB.
	public static  ArrayList<Integer> loadBinaryFile(File file) throws IOException{
		int initCount = (int)FileUtils.sizeOf(file) / 2;
		
		ArrayList<Integer> values = new ArrayList<Integer>(initCount); 
		DataInputStream dis = new DataInputStream(new FileInputStream(file));
		while(dis.available() > 0) {
			int value = (short)dis.readShort();
			value = (int)PreparationTool.map(value, 0, 255, Short.MIN_VALUE, Short.MAX_VALUE);
			values.add(value);
		}
		dis.close();
		return values;
	}
	
	//Copied from the map-method from the arduino-website.
	public static long map(long value, long inMin, long inMax, long outMin, long outMax) {
		return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}
}
