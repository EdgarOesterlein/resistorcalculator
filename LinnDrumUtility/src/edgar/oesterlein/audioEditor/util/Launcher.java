package edgar.oesterlein.audioEditor.util;

import java.awt.EventQueue;

import edgar.oesterlein.audioEditor.controller.MainController;

public class Launcher {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					MainController mainController = new MainController();
					mainController.showView();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
