package edgar.oesterlein.audioEditor.UIComponent;

import javax.swing.JPanel;

import edgar.oesterlein.audioEditor.command.Command;
import edgar.oesterlein.audioEditor.command.CommandListener;
import edgar.oesterlein.audioEditor.command.CommandManager;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JLabel;

public class CmdProtocolPanel extends JPanel implements CommandListener {
	private JPanel top_panel;
	private JPanel center_panel;
	private JPanel bottom_panel;
	private JScrollPane cmdList_scrollPane;
	private JScrollPane redoCmdList_scrollPane;
	private JList cmd_list;
	private DefaultListModel<Command> cmdListModel;
	private JList redoCmd_list;
	private DefaultListModel<Command> redoCmdListModel;
	private JLabel cmdList_lbl;
	
	private CommandManager cmdManager;
	
	/**
	 * Create the panel.
	 */
	public CmdProtocolPanel() {
		cmdManager = CommandManager.getInstance();
		cmdListModel = new DefaultListModel<>();
		redoCmdListModel = new DefaultListModel<>();
		
		setLayout(new BorderLayout(0, 0));
		
		top_panel = new JPanel();
		add(top_panel, BorderLayout.NORTH);
		top_panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		cmdList_lbl = new JLabel("Aktivit\u00E4tenprotokoll:");
		top_panel.add(cmdList_lbl);
		
		center_panel = new JPanel();
		add(center_panel, BorderLayout.CENTER);
		center_panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		cmdList_scrollPane = new JScrollPane();
		center_panel.add(cmdList_scrollPane);
		
		cmd_list = new JList(cmdListModel);
		cmdList_scrollPane.setViewportView(cmd_list);
		
		redoCmdList_scrollPane = new JScrollPane();
		center_panel.add(redoCmdList_scrollPane);
		
		redoCmd_list = new JList(redoCmdListModel);
		redoCmdList_scrollPane.setViewportView(redoCmd_list);
		
		bottom_panel = new JPanel();
		add(bottom_panel, BorderLayout.SOUTH);
		
		cmdManager.addCommandListener(this);
	}

	@Override
	public void commandListUpdated(List<Command> commandList) {
		cmdListModel.clear();
		for(Command c : commandList)
			cmdListModel.addElement(c);
	}

	@Override
	public void redoCommandListUpdated(List<Command> redoCommandList) {
		redoCmdListModel.clear();
		for(Command c : redoCommandList)
			redoCmdListModel.addElement(c);
	}

}
