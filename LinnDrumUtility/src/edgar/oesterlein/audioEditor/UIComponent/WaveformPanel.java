package edgar.oesterlein.audioEditor.UIComponent;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.GeneralPath;
import java.util.List;
import java.lang.IllegalArgumentException;

import javax.swing.JPanel;

import edgar.oesterlein.audioEditor.util.PreparationTool;

/*
 * This class represents a panel on which the waveform is drawn
 * */
public class WaveformPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private List<Integer> ref_values;
	private GeneralPath polyline;
	private Color waveColor;
	
	//private float horizontalZoom = 1f;	//TODO: checken ob gebraucht wird. evtll als feinabstimmung?
	private float verticalZoom = 1f;	//TODO: gilt wie ein faktor

	private int verticalCenter;

	public void setWaveColor(Color waveColor) {
		this.waveColor = waveColor;
	}

	public WaveformPanel(List<Integer> ref_values, Color waveColor) {
		this.setBackground(Color.BLACK);
		this.waveColor = waveColor;
		this.ref_values = ref_values;
		polyline = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ref_values.size());
		//setRenderTrackArea(0, ref_values.size()-1);	//If not otherwise specified.
		setRenderTrackArea(2000, 3000);	//f�r Testzwecke
	}
	
	
	private int startIndex = 0;
	private int endIndex = 0;
		
	public void setRenderTrackArea(int startIndex, int endIndex) {
		StringBuilder errorMessage = new StringBuilder(200);
		int errorCount = 0;
		//Checking conditions. If any is violated an Exception will be thrown.
		if(startIndex < 0) {
			errorMessage.append("VIOLATED CONDITION! IT MUST BE: StartIndex >= 0\n");
			errorCount++;
		}
		if(startIndex >= endIndex) {
			errorMessage.append("VIOLATED CONDITION! IT MUST BE: StartIndex < EndIndex\n");
			errorCount++;
		}
		if(endIndex >= ref_values.size()) {
			errorMessage.append("VIOLATED CONDITION! IT MUST BE: EndIndex <= SIZE_OF_LIST\n");
			errorCount++;
		}
		
		if(errorCount > 0)
			throw new IllegalArgumentException("INVALID RANGE! renderTrackArea(" + startIndex + ", " + endIndex + ");" + errorCount + " condition violations occured.\n" + errorMessage.toString());
		else {
			this.startIndex = startIndex;
			this.endIndex = endIndex;
		}
	}
	
	//Cleans the screen with background color and converting Graphics object to Graphics2D.
	private Graphics2D initializeEnvironment(Graphics g) {
		super.paintComponent(g); 
		return (Graphics2D) g;	
	}
	
	private void paintHorizontalLine(Graphics2D g2) {
		g2.setColor(Color.LIGHT_GRAY);
		g2.drawLine(0, verticalCenter, this.getWidth(), verticalCenter);
	}
	
	private void paintWaveform(Graphics2D g2, boolean crosshairEnabled) {
		polyline.reset();
		g2.setColor(waveColor);
		double horizontalStep = (double)this.getWidth() / (double)(endIndex - startIndex);
		
		
		//Draw the first point of the polyline.
		polyline.moveTo(0, calculateAmplitude(0));
		
		Crosshair crosshair = new Crosshair();
		crosshair.setCoordinates(0, calculateAmplitude(0));
		crosshair.paintCrosshair(g2);
		
		//Draw the remaining points of the polyline.
		int x = 1;
		for(int i = (startIndex+1); i <= endIndex; i++) {
			//Waveform itself
			polyline.lineTo(x*horizontalStep, calculateAmplitude(i));
			
			//Crosshair
			crosshair.setCoordinates((int)(x*horizontalStep), calculateAmplitude(i));
			crosshair.paintCrosshair(g2);
			x++;
		}
		//Draw the final waveform to this panel.
		g2.draw(polyline);
	}
	
	
	
	private int calculateAmplitude(int index) {
		int mappedValue = ((int)PreparationTool.map(ref_values.get(index), Integer.MIN_VALUE, Integer.MAX_VALUE, -verticalCenter, verticalCenter) * (int)verticalZoom);
		return this.getHeight() - (verticalCenter + mappedValue);	//And then position the mappedValue
	}

	private void updateVerticalCenter() {
		verticalCenter = Math.round(this.getHeight() / 2f);
	}
	
	
	@Override
	protected void paintComponent(Graphics g) {
		//Initialization of environment.
		Graphics2D g2 = initializeEnvironment(g);
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		updateVerticalCenter();
		
		//Draw a horizontal line into the center of this panel for better orientation.
		paintHorizontalLine(g2);
		
		//Drawing the waveform with or without crosshair.
		paintWaveform(g2, true);	//TODO: boolean ohne Funktion..
	}
}
