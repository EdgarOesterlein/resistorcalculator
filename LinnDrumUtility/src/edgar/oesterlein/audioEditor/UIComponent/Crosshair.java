package edgar.oesterlein.audioEditor.UIComponent;

import java.awt.Graphics;

public class Crosshair {
	private int x, y, radius;
	
	public void setRadius(int radius) {
		if(radius < 0)
			this.radius = 0;
		else
			this.radius = radius;
	}

	public Crosshair() {
		setCoordinates(0, 0);
		setRadius(1);	//Standard
	}
	
	public void setCoordinates(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	
	
	public void paintCrosshair(Graphics g) {
		g.drawLine(x-radius, y, x+radius, y);	//Horizontal line
		g.drawLine(x, y-radius, x, y+radius);	//Vertical line
	}
}
