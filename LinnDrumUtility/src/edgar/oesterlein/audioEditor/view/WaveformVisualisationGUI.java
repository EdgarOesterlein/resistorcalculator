package edgar.oesterlein.audioEditor.view;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JPanel;

import edgar.oesterlein.audioEditor.UIComponent.WaveformPanel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

public class WaveformVisualisationGUI extends JFrame {
	private static final long serialVersionUID = 1L;
	
	private int WIDTH_PANEL = 1200;
	private int HEIGHT_PANEL = 250;
	
	
	/**
	 * Create the frame.
	 */
	public WaveformVisualisationGUI(WaveformPanel waveformPanel) {
		setSize(WIDTH_PANEL, HEIGHT_PANEL);
		getContentPane().add(waveformPanel);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	/**
	 * @wbp.parser.constructor
	 */
	public WaveformVisualisationGUI(WaveformPanel waveformPanel_L, WaveformPanel waveformPanel_R) {
		setSize(WIDTH_PANEL, HEIGHT_PANEL*2);
		waveformPanel_L.setSize(WIDTH_PANEL, HEIGHT_PANEL);
		waveformPanel_R.setSize(WIDTH_PANEL, HEIGHT_PANEL);
		getContentPane().add(waveformPanel_L);
		getContentPane().add(waveformPanel_R);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		
		GridBagConstraints gbc_panel_L = new GridBagConstraints();
		//gbc_panel_L.insets = new Insets(0, 0, 5, 0);
		gbc_panel_L.fill = GridBagConstraints.BOTH;
		gbc_panel_L.gridx = 0;
		gbc_panel_L.gridy = 0;
		getContentPane().add(waveformPanel_L, gbc_panel_L);
		
		GridBagConstraints gbc_panel_R = new GridBagConstraints();
		gbc_panel_R.fill = GridBagConstraints.BOTH;
		gbc_panel_R.gridx = 0;
		gbc_panel_R.gridy = 1;
		getContentPane().add(waveformPanel_R, gbc_panel_R);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
}
