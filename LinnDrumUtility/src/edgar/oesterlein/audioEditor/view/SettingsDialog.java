package edgar.oesterlein.audioEditor.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTree;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.border.TitledBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;

public class SettingsDialog extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField samplerate_txt;

	private ArrayList<Integer> samplerateList;
	
	/**
	 * Create the dialog.
	 */
	public SettingsDialog() {
		samplerateList = new ArrayList<Integer>(13);
		samplerateList.add(8000);
		
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{120, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JTree tree = new JTree();
			GridBagConstraints gbc_tree = new GridBagConstraints();
			gbc_tree.insets = new Insets(0, 0, 0, 5);
			gbc_tree.fill = GridBagConstraints.BOTH;
			gbc_tree.gridx = 0;
			gbc_tree.gridy = 0;
			contentPanel.add(tree, gbc_tree);
		}
		{
			JPanel cards_panel = new JPanel();
			GridBagConstraints gbc_cards_panel = new GridBagConstraints();
			gbc_cards_panel.fill = GridBagConstraints.BOTH;
			gbc_cards_panel.gridx = 1;
			gbc_cards_panel.gridy = 0;
			contentPanel.add(cards_panel, gbc_cards_panel);
			cards_panel.setLayout(new CardLayout(0, 0));
			{
				JPanel quality_panel = new JPanel();
				cards_panel.add(quality_panel, "name_50825820368193");
				GridBagLayout gbl_quality_panel = new GridBagLayout();
				gbl_quality_panel.columnWidths = new int[]{304, 0};
				gbl_quality_panel.rowHeights = new int[]{218, 0};
				gbl_quality_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
				gbl_quality_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
				quality_panel.setLayout(gbl_quality_panel);
				{
					JPanel groupbox1_panel = new JPanel();
					groupbox1_panel.setBorder(new TitledBorder(null, "Aufnahme-/Import-Parameter", TitledBorder.LEADING, TitledBorder.TOP, null, null));
					GridBagConstraints gbc_groupbox1_panel = new GridBagConstraints();
					gbc_groupbox1_panel.fill = GridBagConstraints.BOTH;
					gbc_groupbox1_panel.gridx = 0;
					gbc_groupbox1_panel.gridy = 0;
					quality_panel.add(groupbox1_panel, gbc_groupbox1_panel);
					GridBagLayout gbl_groupbox1_panel = new GridBagLayout();
					gbl_groupbox1_panel.columnWidths = new int[]{0, 0, 0};
					gbl_groupbox1_panel.rowHeights = new int[]{0, 0, 0};
					gbl_groupbox1_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
					gbl_groupbox1_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
					groupbox1_panel.setLayout(gbl_groupbox1_panel);
					{
						JLabel samplerate_lbl = new JLabel("Standard-Abtastrate:");
						GridBagConstraints gbc_samplerate_lbl = new GridBagConstraints();
						gbc_samplerate_lbl.insets = new Insets(0, 0, 5, 5);
						gbc_samplerate_lbl.anchor = GridBagConstraints.EAST;
						gbc_samplerate_lbl.gridx = 0;
						gbc_samplerate_lbl.gridy = 0;
						groupbox1_panel.add(samplerate_lbl, gbc_samplerate_lbl);
					}
					{
						samplerate_txt = new JTextField();
						samplerate_txt.setText("44100");
						GridBagConstraints gbc_samplerate_txt = new GridBagConstraints();
						gbc_samplerate_txt.insets = new Insets(0, 0, 5, 0);
						gbc_samplerate_txt.fill = GridBagConstraints.HORIZONTAL;
						gbc_samplerate_txt.gridx = 1;
						gbc_samplerate_txt.gridy = 0;
						groupbox1_panel.add(samplerate_txt, gbc_samplerate_txt);
						samplerate_txt.setColumns(10);
					}
					{
						JLabel bitrate_lbl = new JLabel("Standard-Samplerate:");
						GridBagConstraints gbc_bitrate_lbl = new GridBagConstraints();
						gbc_bitrate_lbl.anchor = GridBagConstraints.EAST;
						gbc_bitrate_lbl.insets = new Insets(0, 0, 0, 5);
						gbc_bitrate_lbl.gridx = 0;
						gbc_bitrate_lbl.gridy = 1;
						groupbox1_panel.add(bitrate_lbl, gbc_bitrate_lbl);
					}
					{
						JComboBox bitrate_cmb = new JComboBox(samplerateList.toArray());
						GridBagConstraints gbc_bitrate_cmb = new GridBagConstraints();
						gbc_bitrate_cmb.fill = GridBagConstraints.HORIZONTAL;
						gbc_bitrate_cmb.gridx = 1;
						gbc_bitrate_cmb.gridy = 1;
						groupbox1_panel.add(bitrate_cmb, gbc_bitrate_cmb);
					}
				}
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

}
