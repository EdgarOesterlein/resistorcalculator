package edgar.oesterlein.audioEditor.view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.event.ActionListener;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.border.BevelBorder;
import javax.swing.BoxLayout;
import javax.swing.JSeparator;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private JMenuItem toneGenerator_menuItem, exit_menuItem, settings_menuItem;
	private JMenu tracks_menu;
	private JMenu createNewTrack_menu;
	private JMenuItem monoTrack_menuItem;
	private JMenuItem stereoTrack_menuItem;
	private JMenuItem undo_menuItem;
	private JMenuItem redo_menuItem;
	private JSeparator separator;
	private JPanel south_panel;
	private JPanel north_panel;
	private JPanel center_panel;
	private JLabel status_lbl;
	private JPanel west_panel;
	private JPanel east_panel;
	private JScrollPane center_scrollPane;
	private JPanel trackPanel_panel;
	
	/**
	 * Create the frame.
	 */
	public MainFrame() {
		setTitle("LinnDrumUtility");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu file_menu = new JMenu("Datei");
		menuBar.add(file_menu);
		
		exit_menuItem = new JMenuItem("Beenden");
		file_menu.add(exit_menuItem);
		
		JMenu edit_menu = new JMenu("Bearbeiten");
		menuBar.add(edit_menu);
		
		undo_menuItem = new JMenuItem("R\u00FCckg\u00E4ngig");
		edit_menu.add(undo_menuItem);
		
		redo_menuItem = new JMenuItem("Wiederherstellen");
		edit_menu.add(redo_menuItem);
		
		separator = new JSeparator();
		edit_menu.add(separator);
		
		settings_menuItem = new JMenuItem("Einstellungen...");
		edit_menu.add(settings_menuItem);
		
		tracks_menu = new JMenu("Spuren");
		menuBar.add(tracks_menu);
		
		createNewTrack_menu = new JMenu("Neue Spur erzeugen");
		tracks_menu.add(createNewTrack_menu);
		
		monoTrack_menuItem = new JMenuItem("Monospur");
		createNewTrack_menu.add(monoTrack_menuItem);
		
		stereoTrack_menuItem = new JMenuItem("Stereospur");
		createNewTrack_menu.add(stereoTrack_menuItem);
		
		JMenu generate_menu = new JMenu("Erzeugen");
		menuBar.add(generate_menu);
		
		toneGenerator_menuItem = new JMenuItem("Tongenerator...");
		generate_menu.add(toneGenerator_menuItem);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		south_panel = new JPanel();
		south_panel.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		contentPane.add(south_panel, BorderLayout.SOUTH);
		south_panel.setLayout(new BoxLayout(south_panel, BoxLayout.X_AXIS));
		
		status_lbl = new JLabel("<dynamic>");
		south_panel.add(status_lbl);
		
		north_panel = new JPanel();
		contentPane.add(north_panel, BorderLayout.NORTH);
		north_panel.setLayout(new BoxLayout(north_panel, BoxLayout.X_AXIS));
		north_panel.setVisible(false);
		
		center_panel = new JPanel();
		contentPane.add(center_panel, BorderLayout.CENTER);
		center_panel.setLayout(new BoxLayout(center_panel, BoxLayout.Y_AXIS));
		
		center_scrollPane = new JScrollPane();
		center_scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		center_panel.add(center_scrollPane);
		
		trackPanel_panel = new JPanel();
		center_scrollPane.setViewportView(trackPanel_panel);
		trackPanel_panel.setLayout(new BoxLayout(trackPanel_panel, BoxLayout.Y_AXIS));
		
		west_panel = new JPanel();
		contentPane.add(west_panel, BorderLayout.WEST);
		west_panel.setLayout(new BoxLayout(west_panel, BoxLayout.Y_AXIS));
		west_panel.setVisible(false);
		
		east_panel = new JPanel();
		contentPane.add(east_panel, BorderLayout.EAST);
		east_panel.setVisible(true);
	}
	
	public void setStatus_lbl(String txt) {
		status_lbl.setText(txt);
	}
	
	public void addToEastPanel(JPanel panel) {
		east_panel.add(panel);
	}
	
	public void addTrackPanel(TrackPanel trackPanel) {
		trackPanel_panel.add(trackPanel);
	}
	
	public void removeTrackPanel(TrackPanel trackPanel) {
		center_panel.remove(trackPanel);
	}
	
	public void setVisibilityOfAllTrackPanels(boolean visible) {
		for(Component c : trackPanel_panel.getComponents()) {
			c.setVisible(visible);
		}
	}
	
	public void updateAllTrackPanels() {
		setVisibilityOfAllTrackPanels(false);
		setVisibilityOfAllTrackPanels(true);
	}
	
	////////////////////
	//LISTENER BINDING//
	////////////////////
	
	public void setExit_menuItemListener(ActionListener l) {
		exit_menuItem.addActionListener(l);
	}
	
	public void setUndo_menuItemListener(ActionListener l) {
		undo_menuItem.addActionListener(l);
	}
	
	public void setRedo_menuItemListener(ActionListener l) {
		redo_menuItem.addActionListener(l);
	}
	
	public void setAddMonoTrack_menuItemListener(ActionListener l) {
		monoTrack_menuItem.addActionListener(l);
	}
	
	public void setAddStereoTrack_menuItemListener(ActionListener l) {
		stereoTrack_menuItem.addActionListener(l);
	}
	
	public void setToneGenerator_menuItemListener(ActionListener l) {
		toneGenerator_menuItem.addActionListener(l);
	}
	
	public void setSettings_menuItemListener(ActionListener l) {
		settings_menuItem.addActionListener(l);
	}
}
