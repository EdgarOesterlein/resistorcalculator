package edgar.oesterlein.audioEditor.view;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.Hashtable;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JSlider;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;

import javax.swing.border.BevelBorder;

import edgar.oesterlein.audioEditor.UIComponent.WaveformPanel;

import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.border.LineBorder;

public class TrackPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField trackName_txt;
	private WaveformPanel waveform_panel1, waveform_panel2;
	private JLabel trackType_lbl;
	private JLabel trackBitRate_lbl;
	private JLabel trackSampleRate_lbl;
	private JButton close_btn;
	private JPanel contentPanel;
	
	private Color color = Color.RED;
	private JPanel waveformPanelArea_panel;
	
	/**
	 * Create the panel.
	 */
	public TrackPanel() {
		setLayout(new BorderLayout(0, 0));
		
		contentPanel = new JPanel();
		contentPanel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null, null));
		add(contentPanel, BorderLayout.WEST);
		contentPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel controlArea_panel = new JPanel();
		contentPanel.add(controlArea_panel);
		GridBagLayout gbl_controlArea_panel = new GridBagLayout();
		gbl_controlArea_panel.columnWidths = new int[]{0, 0};
		gbl_controlArea_panel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_controlArea_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_controlArea_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		controlArea_panel.setLayout(gbl_controlArea_panel);
		
		JPanel controlAreaTop_panel = new JPanel();
		GridBagConstraints gbc_controlAreaTop_panel = new GridBagConstraints();
		gbc_controlAreaTop_panel.insets = new Insets(0, 0, 5, 0);
		gbc_controlAreaTop_panel.fill = GridBagConstraints.BOTH;
		gbc_controlAreaTop_panel.gridx = 0;
		gbc_controlAreaTop_panel.gridy = 0;
		controlArea_panel.add(controlAreaTop_panel, gbc_controlAreaTop_panel);
		GridBagLayout gbl_controlAreaTop_panel = new GridBagLayout();
		gbl_controlAreaTop_panel.columnWidths = new int[]{0, 0, 0};
		gbl_controlAreaTop_panel.rowHeights = new int[]{0, 0};
		gbl_controlAreaTop_panel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_controlAreaTop_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		controlAreaTop_panel.setLayout(gbl_controlAreaTop_panel);
		
		close_btn = new JButton("X");
		GridBagConstraints gbc_close_btn = new GridBagConstraints();
		gbc_close_btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_close_btn.insets = new Insets(0, 0, 0, 5);
		gbc_close_btn.gridx = 0;
		gbc_close_btn.gridy = 0;
		controlAreaTop_panel.add(close_btn, gbc_close_btn);
		
		trackName_txt = new JTextField();
		trackName_txt.setText("name");
		GridBagConstraints gbc_trackName_txt = new GridBagConstraints();
		gbc_trackName_txt.fill = GridBagConstraints.HORIZONTAL;
		gbc_trackName_txt.gridx = 1;
		gbc_trackName_txt.gridy = 0;
		controlAreaTop_panel.add(trackName_txt, gbc_trackName_txt);
		trackName_txt.setColumns(10);
		
		JPanel trackinfo_panel = new JPanel();
		GridBagConstraints gbc_trackinfo_panel = new GridBagConstraints();
		gbc_trackinfo_panel.insets = new Insets(0, 0, 5, 0);
		gbc_trackinfo_panel.fill = GridBagConstraints.BOTH;
		gbc_trackinfo_panel.gridx = 0;
		gbc_trackinfo_panel.gridy = 1;
		controlArea_panel.add(trackinfo_panel, gbc_trackinfo_panel);
		GridBagLayout gbl_trackinfo_panel = new GridBagLayout();
		gbl_trackinfo_panel.columnWidths = new int[]{0, 0, 0};
		gbl_trackinfo_panel.rowHeights = new int[]{0, 0, 0};
		gbl_trackinfo_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_trackinfo_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		trackinfo_panel.setLayout(gbl_trackinfo_panel);
		
		trackType_lbl = new JLabel("trackType");
		GridBagConstraints gbc_trackType_lbl = new GridBagConstraints();
		gbc_trackType_lbl.insets = new Insets(0, 0, 5, 5);
		gbc_trackType_lbl.gridx = 0;
		gbc_trackType_lbl.gridy = 0;
		trackinfo_panel.add(trackType_lbl, gbc_trackType_lbl);
		
		trackSampleRate_lbl = new JLabel("sampleRate");
		GridBagConstraints gbc_trackSampleRate_lbl = new GridBagConstraints();
		gbc_trackSampleRate_lbl.insets = new Insets(0, 0, 5, 0);
		gbc_trackSampleRate_lbl.gridx = 1;
		gbc_trackSampleRate_lbl.gridy = 0;
		trackinfo_panel.add(trackSampleRate_lbl, gbc_trackSampleRate_lbl);
		
		trackBitRate_lbl = new JLabel("bitRate");
		GridBagConstraints gbc_trackBitRate_lbl = new GridBagConstraints();
		gbc_trackBitRate_lbl.insets = new Insets(0, 0, 0, 5);
		gbc_trackBitRate_lbl.gridx = 0;
		gbc_trackBitRate_lbl.gridy = 1;
		trackinfo_panel.add(trackBitRate_lbl, gbc_trackBitRate_lbl);
		
		JPanel muteSolo_panel = new JPanel();
		GridBagConstraints gbc_muteSolo_panel = new GridBagConstraints();
		gbc_muteSolo_panel.insets = new Insets(0, 0, 5, 0);
		gbc_muteSolo_panel.fill = GridBagConstraints.BOTH;
		gbc_muteSolo_panel.gridx = 0;
		gbc_muteSolo_panel.gridy = 2;
		controlArea_panel.add(muteSolo_panel, gbc_muteSolo_panel);
		GridBagLayout gbl_muteSolo_panel = new GridBagLayout();
		gbl_muteSolo_panel.columnWidths = new int[]{0, 0, 0};
		gbl_muteSolo_panel.rowHeights = new int[]{0, 0};
		gbl_muteSolo_panel.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_muteSolo_panel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		muteSolo_panel.setLayout(gbl_muteSolo_panel);
		
		JButton mute_btn = new JButton("Stumm");
		GridBagConstraints gbc_mute_btn = new GridBagConstraints();
		gbc_mute_btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_mute_btn.insets = new Insets(0, 0, 0, 5);
		gbc_mute_btn.gridx = 0;
		gbc_mute_btn.gridy = 0;
		muteSolo_panel.add(mute_btn, gbc_mute_btn);
		
		JButton solo_btn = new JButton("Solo");
		GridBagConstraints gbc_solo_btn = new GridBagConstraints();
		gbc_solo_btn.fill = GridBagConstraints.HORIZONTAL;
		gbc_solo_btn.gridx = 1;
		gbc_solo_btn.gridy = 0;
		muteSolo_panel.add(solo_btn, gbc_solo_btn);
		
		JPanel amplitudeBalance_panel = new JPanel();
		GridBagConstraints gbc_amplitudeBalance_panel = new GridBagConstraints();
		gbc_amplitudeBalance_panel.fill = GridBagConstraints.BOTH;
		gbc_amplitudeBalance_panel.gridx = 0;
		gbc_amplitudeBalance_panel.gridy = 3;
		controlArea_panel.add(amplitudeBalance_panel, gbc_amplitudeBalance_panel);
		GridBagLayout gbl_amplitudeBalance_panel = new GridBagLayout();
		gbl_amplitudeBalance_panel.columnWidths = new int[]{0, 0};
		gbl_amplitudeBalance_panel.rowHeights = new int[]{0, 0, 0};
		gbl_amplitudeBalance_panel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_amplitudeBalance_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		amplitudeBalance_panel.setLayout(gbl_amplitudeBalance_panel);
		
		JSlider amplification_slider = new JSlider();
		amplification_slider.setPaintLabels(true);
		amplification_slider.setValue(0);
		amplification_slider.setMinimum(-100);
		amplification_slider.setSnapToTicks(true);
		amplification_slider.setMinorTickSpacing(10);
		amplification_slider.setPaintTicks(true);
		GridBagConstraints gbc_amplification_slider = new GridBagConstraints();
		gbc_amplification_slider.insets = new Insets(0, 0, 5, 0);
		gbc_amplification_slider.gridx = 0;
		gbc_amplification_slider.gridy = 0;
		amplitudeBalance_panel.add(amplification_slider, gbc_amplification_slider);
		
		Hashtable<Integer, JLabel> ampLabelTable = new Hashtable<Integer, JLabel>();
		ampLabelTable.put(new Integer(amplification_slider.getMinimum()), new JLabel("-"));
		ampLabelTable.put(new Integer(amplification_slider.getMaximum()), new JLabel("+"));
		amplification_slider.setLabelTable(ampLabelTable);
		
		JSlider panorama_slider = new JSlider();
		panorama_slider.setPaintLabels(true);
		panorama_slider.setSnapToTicks(true);
		panorama_slider.setPaintTicks(true);
		panorama_slider.setMinorTickSpacing(10);
		panorama_slider.setValue(0);
		panorama_slider.setMinimum(-100);
		GridBagConstraints gbc_panorama_slider = new GridBagConstraints();
		gbc_panorama_slider.gridx = 0;
		gbc_panorama_slider.gridy = 1;
		amplitudeBalance_panel.add(panorama_slider, gbc_panorama_slider);
		
		waveformPanelArea_panel = new JPanel();
		add(waveformPanelArea_panel, BorderLayout.CENTER);
		waveformPanelArea_panel.setLayout(new BoxLayout(waveformPanelArea_panel, BoxLayout.Y_AXIS));
		
		Hashtable<Integer, JLabel> panLabelTable = new Hashtable<Integer, JLabel>();
		panLabelTable.put(new Integer(panorama_slider.getMinimum()), new JLabel("L"));
		panLabelTable.put(new Integer(panorama_slider.getMaximum()), new JLabel("R"));
		panorama_slider.setLabelTable(panLabelTable);
	}

	public void bindDataToWaveformPanel(List<Integer> ref_values) {
		waveform_panel1 = new WaveformPanel(ref_values, color); //TODO: Farben dynamisch �ndern
		waveformPanelArea_panel.add(waveform_panel1);
		waveform_panel1.setVisible(true);
		//waveform_panel.repaint();
	}
	
	public void bindDataToWaveformPanel(List<Integer> ref_values1, List<Integer> ref_values2) {
		bindDataToWaveformPanel(ref_values1);	//TODO: Farben dynamisch �ndern
		waveform_panel2 = new WaveformPanel(ref_values2, color);
		waveformPanelArea_panel.add(waveform_panel2);
		waveform_panel2.setVisible(true);
	}
	
	public void setWaveColor(Color waveColor) {
		waveform_panel1.setWaveColor(waveColor);
		if(waveform_panel2 != null)	//If stereo-channel
			waveform_panel2.setWaveColor(waveColor);
	}
	
	public String getTrackName_txt() {
		return trackName_txt.getText();
	}
	
	public void setTrackName_txt(String txt) {
		trackName_txt.setText(txt);
	}
	
	public void setTrackType_txt(String txt) {
		trackType_lbl.setText(txt);
	}
	
	public void setTrackSampleRate_lbl(String txt) {
		trackSampleRate_lbl.setText(txt);
	}
	
	public void setTrackBitRate_lbl(String txt) {
		trackBitRate_lbl.setText(txt);
	}
		
	public void setTrackPanelSelection(boolean trackSelected) {
		if(trackSelected) 
			setBorder(new LineBorder(Color.BLUE, 3));
		else
			setBorder(null);
	}
	
	////////////////////
	//LISTENER BINDING//
	////////////////////
	
	public void setTrackPanelMouseListener(MouseListener l) {
		this.addMouseListener(l);
	}
	
	public void setClose_btnListener(ActionListener l) {
		close_btn.addActionListener(l);
	}
	
	public void setTrackName_txtListener(ActionListener l) {
		trackName_txt.addActionListener(l);
	}
}
