package edgar.oesterlein.audioEditor.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Frame;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;

import edgar.oesterlein.audioEditor.model.Waveform;

public class ToneGeneratorDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final JPanel contentPanel = new JPanel();
	
	private JTextField frequency_txt;
	private JTextField amplitude_txt;
	private JTextField durationOfRecording_txt;
	
	private JButton ok_btn, cancel_btn;
	
	private JComboBox<Waveform> waveform_cmb;

	/**
	 * Create the dialog.
	 */
	public ToneGeneratorDialog(Frame frame, boolean modal) {
		super(frame, modal);
		//setModalityType(ModalityType.APPLICATION_MODAL);
		setTitle("Tongenerator");
		setResizable(false);
		setBounds(100, 100, 300, 170);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JLabel waveform_lbl = new JLabel("Wellenform:");
			GridBagConstraints gbc_waveform_lbl = new GridBagConstraints();
			gbc_waveform_lbl.insets = new Insets(0, 0, 5, 5);
			gbc_waveform_lbl.anchor = GridBagConstraints.EAST;
			gbc_waveform_lbl.gridx = 0;
			gbc_waveform_lbl.gridy = 0;
			contentPanel.add(waveform_lbl, gbc_waveform_lbl);
		}
		{
			waveform_cmb = new JComboBox<Waveform>();
			waveform_cmb.setModel(new DefaultComboBoxModel<Waveform>(Waveform.values()));
			GridBagConstraints gbc_waveform_cmb = new GridBagConstraints();
			gbc_waveform_cmb.insets = new Insets(0, 0, 5, 0);
			gbc_waveform_cmb.fill = GridBagConstraints.HORIZONTAL;
			gbc_waveform_cmb.gridx = 1;
			gbc_waveform_cmb.gridy = 0;
			contentPanel.add(waveform_cmb, gbc_waveform_cmb);
		}
		{
			JLabel frequency_lbl = new JLabel("Frequenz (Hz):");
			GridBagConstraints gbc_frequency_lbl = new GridBagConstraints();
			gbc_frequency_lbl.insets = new Insets(0, 0, 5, 5);
			gbc_frequency_lbl.anchor = GridBagConstraints.EAST;
			gbc_frequency_lbl.gridx = 0;
			gbc_frequency_lbl.gridy = 1;
			contentPanel.add(frequency_lbl, gbc_frequency_lbl);
		}
		{
			frequency_txt = new JTextField();
			frequency_txt.setText("440");
			GridBagConstraints gbc_frequency_txt = new GridBagConstraints();
			gbc_frequency_txt.insets = new Insets(0, 0, 5, 0);
			gbc_frequency_txt.fill = GridBagConstraints.HORIZONTAL;
			gbc_frequency_txt.gridx = 1;
			gbc_frequency_txt.gridy = 1;
			contentPanel.add(frequency_txt, gbc_frequency_txt);
			frequency_txt.setColumns(10);
		}
		{
			JLabel amplitude_lbl = new JLabel("Amplitude (0-1):");
			GridBagConstraints gbc_amplitude_lbl = new GridBagConstraints();
			gbc_amplitude_lbl.anchor = GridBagConstraints.EAST;
			gbc_amplitude_lbl.insets = new Insets(0, 0, 5, 5);
			gbc_amplitude_lbl.gridx = 0;
			gbc_amplitude_lbl.gridy = 2;
			contentPanel.add(amplitude_lbl, gbc_amplitude_lbl);
		}
		{
			amplitude_txt = new JTextField();
			amplitude_txt.setText("0.8");
			GridBagConstraints gbc_amplitude_txt = new GridBagConstraints();
			gbc_amplitude_txt.insets = new Insets(0, 0, 5, 0);
			gbc_amplitude_txt.fill = GridBagConstraints.HORIZONTAL;
			gbc_amplitude_txt.gridx = 1;
			gbc_amplitude_txt.gridy = 2;
			contentPanel.add(amplitude_txt, gbc_amplitude_txt);
			amplitude_txt.setColumns(10);
		}
		{
			JLabel durationOfRecording_lbl = new JLabel("Dauer der Aufnahme (Sekunde):");
			GridBagConstraints gbc_durationOfRecording_lbl = new GridBagConstraints();
			gbc_durationOfRecording_lbl.anchor = GridBagConstraints.EAST;
			gbc_durationOfRecording_lbl.insets = new Insets(0, 0, 0, 5);
			gbc_durationOfRecording_lbl.gridx = 0;
			gbc_durationOfRecording_lbl.gridy = 3;
			contentPanel.add(durationOfRecording_lbl, gbc_durationOfRecording_lbl);
		}
		{
			durationOfRecording_txt = new JTextField();
			durationOfRecording_txt.setText("3");
			GridBagConstraints gbc_durationOfRecording_txt = new GridBagConstraints();
			gbc_durationOfRecording_txt.fill = GridBagConstraints.HORIZONTAL;
			gbc_durationOfRecording_txt.gridx = 1;
			gbc_durationOfRecording_txt.gridy = 3;
			contentPanel.add(durationOfRecording_txt, gbc_durationOfRecording_txt);
			durationOfRecording_txt.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				ok_btn = new JButton("OK");
				ok_btn.setActionCommand("OK");
				buttonPane.add(ok_btn);
				getRootPane().setDefaultButton(ok_btn);
			}
			{
				cancel_btn = new JButton("Cancel");
				cancel_btn.setActionCommand("Cancel");
				buttonPane.add(cancel_btn);
			}
		}
	}

	public Waveform getWaveform() {
		return (Waveform)waveform_cmb.getSelectedItem();
	}
	
	public String getFrequency() {
		return frequency_txt.getText();
	}
	
	public String getAmplitudeFactor() {
		return amplitude_txt.getText();
	}
	
	//TODO: This has to be specified further... (Second, Hours, Milliseconds?)
	public String getDurationOfRecording() {
		return durationOfRecording_txt.getText();
	}
	
	public void setOk_btnListener(ActionListener l) {
		ok_btn.addActionListener(l);
	}
	
	public void setCancel_btnListener(ActionListener l) {
		cancel_btn.addActionListener(l);
	}
}
