package edgar.oesterlein.audioEditor.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import edgar.oesterlein.audioEditor.UIComponent.CmdProtocolPanel;
import edgar.oesterlein.audioEditor.audioProcessing.TrackListListener;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.command.AddEmptyMonoTrackCommand;
import edgar.oesterlein.audioEditor.command.AddEmptyStereoTrackCommand;
import edgar.oesterlein.audioEditor.command.Command;
import edgar.oesterlein.audioEditor.command.CommandManager;
import edgar.oesterlein.audioEditor.command.RemoveTrackCommand;
import edgar.oesterlein.audioEditor.model.MainModel;
import edgar.oesterlein.audioEditor.model.MainModelImpl;
import edgar.oesterlein.audioEditor.model.Settings;
import edgar.oesterlein.audioEditor.util.ErrorReport;
import edgar.oesterlein.audioEditor.view.MainFrame;

public class MainController implements TrackListListener{
	private MainFrame mainFrame;
	private CmdProtocolPanel cmdProtocolPanel;
	private MainModelImpl mainModel;
	
	private List<TrackController> trackControllerList = new ArrayList<>(MainModel.TRACKLISTSIZE_INIT);
	private ToneGeneratorController toneGeneratorController;
	
	//Commands
	Command removeTrackCommand;
	Command addEmptyMonoTrackCommand, addEmptyStereoTrackCommand;
	Command addCopyTrackCommand;
	CommandManager cmdManager;
	
	public MainController() {
		mainFrame = new MainFrame();
		mainModel = new MainModelImpl();
		mainModel.addTrackListListener(this);
		
		//Commands
		addEmptyMonoTrackCommand = new AddEmptyMonoTrackCommand(mainModel);
		addEmptyStereoTrackCommand = new AddEmptyStereoTrackCommand(mainModel);
		cmdManager = CommandManager.getInstance();
		
		
		//Load settings
		//TODO: Fehler gescheit behandeln.
		try {
			mainModel.loadSettings();
		} catch (IOException e) {
			ErrorReport.saveReport(e);
		} catch (ClassNotFoundException e) {
			ErrorReport.saveReport(e);
		}
		
		cmdProtocolPanel = new CmdProtocolPanel();
		mainFrame.addToEastPanel(cmdProtocolPanel);
		cmdProtocolPanel.setVisible(true);
		addListener();
	}
	
	
	private void addListener() {
		mainFrame.setExit_menuItemListener(new Exit_menuItemListener());
		mainFrame.setUndo_menuItemListener(new Undo_menuItemListener());
		mainFrame.setRedo_menuItemListener(new Redo_menuItemListener());
		mainFrame.setAddMonoTrack_menuItemListener(new AddMonoTrack_menuItemListener());
		mainFrame.setAddStereoTrack_menuItemListener(new AddStereoTrack_menuItemListener());
		mainFrame.setToneGenerator_menuItemListener(new ToneGenerator_menuItemListener());
		mainFrame.setSettings_menuItemListener(new Settings_menuItemListener());
	}

	public void showView() {
		mainFrame.setVisible(true);
	}
	
	@Override
	public void trackAdded(Track track) {
		TrackController trackController = new TrackController(mainModel, track);
		mainFrame.addTrackPanel(trackController.getView());
		trackController.showView();
		trackControllerList.add(trackController);
	}

	@Override
	public void trackRemoved(Track track) {
		//Search for the Track Controller with the same Track and then 
		//remove the Track Controller from the list, dispose the view
		for(TrackController trackController : trackControllerList) 
			if(trackController.getTrack().equals(track)) {
				trackControllerList.remove(trackController);
				trackController.close();
				trackController = null;
				mainFrame.setStatus_lbl("Track '" + track.getName() + "' wurde gel�scht.");
				return;
			}
	}
	
	///////////////////////
	//LISTENER GENERATION//
	///////////////////////

	class Exit_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			mainFrame.dispose();
			mainModel = null;
			System.exit(0);
		}
	}
	
	class Undo_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdManager.undo();
		}
	}
	
	class Redo_menuItemListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			cmdManager.redo();
		}
		
	}
	
	//Adds an empty track into the model and adds it as a row into the center of the MainView.
	class AddMonoTrack_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {	
			cmdManager.executeCommand(addEmptyMonoTrackCommand);
			mainFrame.setStatus_lbl("Leeres MonoTrack erstellt, Tracks Gesamtanzahl: " + mainModel.tracksCreatedCount());
		}
	}
	
	//Adds an empty track into the model and adds it as a row into the center of the MainView.
	class AddStereoTrack_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdManager.executeCommand(addEmptyStereoTrackCommand);
			mainFrame.setStatus_lbl("Leeres StereoTrack erstellt, Tracks Gesamtanzahl: " + mainModel.tracksCreatedCount());
		}
	}
	
	class ToneGenerator_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if(mainModel.getSelectedTracksCount() == 0) {
				cmdManager.executeCommand(addEmptyMonoTrackCommand);
			}
			
			toneGeneratorController = new ToneGeneratorController(mainFrame, mainModel.getSelectedTracks());
			toneGeneratorController.showView();
			
			//All Tracks will be redrawn.
			mainFrame.updateAllTrackPanels();
		}
	}

	class Settings_menuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			
		}
	}
}
