package edgar.oesterlein.audioEditor.controller;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.command.Command;
import edgar.oesterlein.audioEditor.command.CommandManager;
import edgar.oesterlein.audioEditor.command.ToneGenerationCommand;
import edgar.oesterlein.audioEditor.model.ToneGeneratorModel;
import edgar.oesterlein.audioEditor.model.Waveform;
import edgar.oesterlein.audioEditor.view.MainFrame;
import edgar.oesterlein.audioEditor.view.ToneGeneratorDialog;

public class ToneGeneratorController {
	private ToneGeneratorDialog toneGeneratorDialog;
	private ToneGeneratorModel toneGeneratorModel;
	
	//Commands
	private CommandManager cmdManager = CommandManager.getInstance();
	private Command toneGenerationCommand;
	
	//This Dialog is modal
	public ToneGeneratorController(Frame mainFrame, List<Track> selectedTracks) {
		toneGeneratorDialog = new ToneGeneratorDialog(mainFrame, true);
		toneGeneratorModel = new ToneGeneratorModel(selectedTracks);
		
		toneGenerationCommand = new ToneGenerationCommand(toneGeneratorModel);
		addListener();
	}
	
	private void addListener() {
		toneGeneratorDialog.setOk_btnListener(new Ok_btnListener());
		toneGeneratorDialog.setCancel_btnListener(new Cancel_btnListener());
	}
	
	public void showView() {
		toneGeneratorDialog.setVisible(true);
	}

	
	class Ok_btnListener implements ActionListener {
		//TODO: Bitrate berücksichtigen...
		@Override
		public void actionPerformed(ActionEvent e) {
			toneGeneratorModel.setWaveform(toneGeneratorDialog.getWaveform());
			toneGeneratorModel.setFrequency(Double.parseDouble(toneGeneratorDialog.getFrequency()));
			toneGeneratorModel.setAmplitudeFactor(Double.parseDouble(toneGeneratorDialog.getAmplitudeFactor()));
			toneGeneratorModel.setDurationOfRecording(Double.parseDouble(toneGeneratorDialog.getDurationOfRecording()));
			
			cmdManager.executeCommand(toneGenerationCommand);
			
			toneGeneratorDialog.dispose();
			toneGeneratorModel = null;
		}
	}
	
	class Cancel_btnListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			toneGeneratorDialog.dispose();
			toneGeneratorModel = null;
		}
	}
}
