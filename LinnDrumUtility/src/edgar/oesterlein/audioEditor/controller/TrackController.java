package edgar.oesterlein.audioEditor.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.TrackListListener;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.audioProcessing.track.TrackSelectionListener;
import edgar.oesterlein.audioEditor.command.Command;
import edgar.oesterlein.audioEditor.command.CommandManager;
import edgar.oesterlein.audioEditor.command.RemoveTrackCommand;
import edgar.oesterlein.audioEditor.model.MainModel;
import edgar.oesterlein.audioEditor.view.TrackPanel;

public class TrackController implements TrackSelectionListener{
	private Track track;	//this is the model for this controller
	private MainModel mainModel_ref;
	private TrackPanel trackPanel;	//this is the view
	
	private CommandManager cmdManager = CommandManager.getInstance();
	private Command removeTrackCommand;
	
	//The reference is passed to the model.
	public TrackController(MainModel mainModel_ref, Track track) {
		this.mainModel_ref = mainModel_ref;
		this.track = track;
		this.track.addTrackSelectionListener(this);
		removeTrackCommand = new RemoveTrackCommand(mainModel_ref, this.track);
		trackPanel = new TrackPanel();
		trackPanel.setTrackName_txt(track.getName());
		trackPanel.setTrackType_txt(track.getTrackChannelType().toString());
		trackPanel.setTrackSampleRate_lbl(String.valueOf(track.getSampleRate()) + " Hz");
		trackPanel.setTrackBitRate_lbl(String.valueOf(track.getBitRate()) + " bit");
		
		if(track.getTrackChannelType() == TrackChannelType.MONO) 
			trackPanel.bindDataToWaveformPanel(track.toMonoTrack().getValues());
		else if(track.getTrackChannelType() == TrackChannelType.STEREO)
			trackPanel.bindDataToWaveformPanel(track.toStereoTrack().getValuesLeftChannel(), track.toStereoTrack().getValuesLeftChannel());
		addListener();
	}
	
	public void showView() {
		trackPanel.setVisible(true);
	}

	public TrackPanel getView() {
		return trackPanel;
	}
	
	//Equals to getModel()
	public Track getTrack() {
		return track;
	}
	
	private void addListener() {
		trackPanel.setTrackName_txtListener(new TrackName_txtListener());
		trackPanel.setClose_btnListener(new Close_btnListener());
		trackPanel.setTrackPanelMouseListener(new TrackPanelMouseListener());
	}
	
	///////////////////////
	//LISTENER GENERATION//
	///////////////////////
	
	class TrackName_txtListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			track.setName(trackPanel.getTrackName_txt());
		}
	}
	
	class Close_btnListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			cmdManager.executeCommand(removeTrackCommand);
			//trackPanel.setVisible(false);
			//trackPanel = null;
		}
	}
	
	class TrackPanelMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if(e.isControlDown())
				track.setSelected(true);
			else {
				mainModel_ref.deselectAllTracks();
				track.setSelected(true);
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}

	//TODO: pr�fen ob remove-command nicht passender w�re...
	public void close() {
		track.setSelected(false);
		trackPanel.removeAll();
		trackPanel = null;
	}

	@Override
	public void updateTrackSelection(boolean trackSelected) {
		trackPanel.setTrackPanelSelection(trackSelected);
	}
}
