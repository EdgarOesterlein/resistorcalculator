package edgar.oesterlein.audioEditor.audioProcessing.track;

import java.util.ArrayList;
import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;

public abstract class Track {
	public static final int INITIAL_CAPACITY =  44100*3;	//TODO: irgendwas anderes einfallen lassen...
	
	private boolean mute = false;
	private boolean solo = false;
	private boolean selected = false;
	
	private String name = "";
	private int sampleRate, bitRate;
	
	private TrackChannelType trackChannelType;
	
	private List<TrackSelectionListener> trackSelectionListenerList = new ArrayList<>(5);
	
	public void addTrackSelectionListener(TrackSelectionListener l) {
		trackSelectionListenerList.add(l);
	}
	
	public void removeTrackSelectionListener(TrackSelectionListener l) {
		trackSelectionListenerList.add(l);
	}
	
	private void notifyTrackSelectionListener() {
		for(TrackSelectionListener l : trackSelectionListenerList)
			l.updateTrackSelection(isSelected());
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		notifyTrackSelectionListener();
	}

	public boolean isSolo() {
		return solo;
	}

	public void setSolo(boolean solo) {
		this.solo = solo;
	}

	public boolean isMute() {
		return mute;
	}

	public void setMute(boolean mute) {
		this.mute = mute;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public int getBitRate() {
		return bitRate;
	}

	public TrackChannelType getTrackChannelType() {
		return trackChannelType;
	}
	
	public Track(TrackChannelType trackChannelType, int sampleRate, int bitRate) {
		this.sampleRate = sampleRate;
		this.bitRate = bitRate;
		this.trackChannelType = trackChannelType;
	}
	
	//Copy-Constructor
	public Track(Track track) {
		mute = track.mute;
		solo = track.solo;
		selected = track.selected;
		name = track.name;
		sampleRate = track.sampleRate;
		bitRate = track.bitRate;
		trackChannelType = track.trackChannelType;
		trackSelectionListenerList = new ArrayList<>(track.trackSelectionListenerList);
	}
	
	public abstract Track clone();
	
	/*
	public TrackChannelType getChannelType() throws Exception{
		if(this.getClass() == MonoTrackImpl.class) 
			return TrackChannelType.MONO;
		else if(this.getClass() == StereoTrackImpl.class)
			return TrackChannelType.STEREO;
		else 
			throw new ChannelTypeNotFoundException();
	}
	*/
	
	//Fills the given track-values with zeros.
	//If some values already exist, just these values will be set to zero.
	//If not then the range from 0 to the initial capacity is affected.
	protected void initializeValues(List<Integer> values) {
		if(values.isEmpty())
			for(int i = 0; i < Track.INITIAL_CAPACITY; i++)
				values.add(0);
		else
			for(int i = 0; i < values.size(); i++)
				values.set(i, 0);
	}
	
	
	//Deletes the part of this track that has been selected. 
	//The deleted part will be returned if the user wants to redo his action.
	public abstract Track deletePart(int startIndex, int endIndex);
	
	//Returns the part of this track that has been selected 
	//Instead of returning a reference of the affected values an exact copy will be returned.
	public abstract Track copyPart(int startIndex, int endIndex);
	
	//public Track cutPart(int startIndex, int endIndex) {
	//	
	//}
	
	//Inserts a track-snippet before the index of this track. 
	//If this track is stereo and the snippet is mono, then the monopart will be copied 
	public abstract void paste(int index, MonoTrack track);
	public abstract void paste(int index, StereoTrack track);
	public abstract MonoTrack toMonoTrack();
	public abstract StereoTrack toStereoTrack();
}
