package edgar.oesterlein.audioEditor.audioProcessing.track;

import java.util.ArrayList;
import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;

public class MonoTrack extends Track{
	private List<Integer> values;
	
	public List<Integer> getValues() {
		return values;
	}
	
	public void setValues(List<Integer> values) {
		this.values = values;
	}

	public MonoTrack(int sampleRate, int bitRate) {
		super(TrackChannelType.MONO, sampleRate, bitRate);
		values = new ArrayList<Integer>(Track.INITIAL_CAPACITY);
		initializeValues(values);
	}
	
	//Copy-Constructor
	public MonoTrack(MonoTrack monoTrack) {
		super(monoTrack);
		values = new ArrayList<>(monoTrack.getValues());
	}
	
	@Override
	public MonoTrack deletePart(int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	//Returns the copied part as a track.
	@Override
	public MonoTrack copyPart(int startIndex, int endIndex) {
		MonoTrack copiedPart = new MonoTrack(this.getSampleRate(), this.getBitRate());
		copiedPart.setValues(values.subList(startIndex, endIndex));
		return copiedPart;
	}

	@Override
	public void paste(int index, MonoTrack monoTrack) {
		List<Integer> resultingTrack = new ArrayList<Integer>(values.size() + monoTrack.getValues().size());
		resultingTrack.addAll(values.subList(0, index));
		resultingTrack.addAll(monoTrack.getValues());
		resultingTrack.addAll(values.subList(index+1, values.size()));
		values = resultingTrack;
	}

	@Override
	public void paste(int index, StereoTrack stereoTrack) {
		paste(index, stereoTrack.toMonoTrack());
	}	
	
	public StereoTrack toStereoTrack() {
		List<Integer> leftChannelValues = new ArrayList<>(values);
		List<Integer> rightChannelValues = new ArrayList<>(values);
		StereoTrack stereoTrack = new StereoTrack(this.getSampleRate(), this.getBitRate()); 
		stereoTrack.setValuesLeftChannel(leftChannelValues);
		stereoTrack.setValuesRightChannel(rightChannelValues);
		return stereoTrack;
	}

	@Override
	public MonoTrack toMonoTrack() {
		return this;
	}

	@Override
	public Track clone() {
		// TODO Auto-generated method stub
		return null;
	}
}
