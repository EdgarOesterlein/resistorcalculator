package edgar.oesterlein.audioEditor.audioProcessing.track;

public interface TrackSelectionListener {
	void updateTrackSelection(boolean trackSelection);
}
