package edgar.oesterlein.audioEditor.audioProcessing.track;

import java.util.ArrayList;
import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;

public class StereoTrack extends Track {
	private List<Integer> valuesLeftChannel, valuesRightChannel;
	
	public StereoTrack(int sampleRate, int bitRate) {
		super(TrackChannelType.STEREO, sampleRate, bitRate);
		valuesLeftChannel = new ArrayList<>(Track.INITIAL_CAPACITY);
		valuesRightChannel = new ArrayList<>(Track.INITIAL_CAPACITY);
		initializeValues(valuesLeftChannel);
		initializeValues(valuesRightChannel);
	}
	
	//Copy-Constructor
	public StereoTrack(StereoTrack stereoTrack) {
		super(stereoTrack);
		valuesLeftChannel = new ArrayList<>(stereoTrack.getValuesLeftChannel());
		valuesRightChannel = new ArrayList<>(stereoTrack.getValuesRightChannel());
	}

	public List<Integer> getValuesLeftChannel() {
		return valuesLeftChannel;
	}



	public void setValuesLeftChannel(List<Integer> valuesLeftChannel) {
		this.valuesLeftChannel = valuesLeftChannel;
	}



	public List<Integer> getValuesRightChannel() {
		return valuesRightChannel;
	}



	public void setValuesRightChannel(List<Integer> valuesRightChannel) {
		this.valuesRightChannel = valuesRightChannel;
	}

	public int size() { 
		//if(valuesLeftChannel.size() == valuesRightChannel.size()) 
			return valuesLeftChannel.size();
	}

	private List<Integer> mixChannelsToMonoChannel() {
		int channelSize = size();
		List<Integer> monoChannel = new ArrayList<>(channelSize);
		for(int i = 0; i < channelSize; i++) 
			monoChannel.add((valuesLeftChannel.get(i) + valuesRightChannel.get(i)) / 2);
		
		return monoChannel;
	}

	@Override
	public Track deletePart(int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public StereoTrack copyPart(int startIndex, int endIndex) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void paste(int index, MonoTrack monoTrack){
		paste(index, monoTrack.toStereoTrack());
	}



	@Override
	public void paste(int index, StereoTrack stereoTrack) {
		List<Integer> resultingLeftChannel = new ArrayList<Integer>(stereoTrack.size() + size());
		List<Integer> resultingRightChannel = new ArrayList<Integer>(stereoTrack.size() + size());
		
		resultingLeftChannel.addAll(valuesLeftChannel.subList(0, index));
		resultingLeftChannel.addAll(stereoTrack.getValuesLeftChannel().subList(0, size()));
		resultingLeftChannel.addAll(valuesLeftChannel.subList(index+1, size()));
		
		resultingRightChannel.addAll(valuesRightChannel.subList(0, index));
		resultingRightChannel.addAll(stereoTrack.getValuesRightChannel().subList(0, size()));
		resultingRightChannel.addAll(valuesRightChannel.subList(index+1, size()));
		
		valuesLeftChannel = resultingLeftChannel;
		valuesRightChannel = resultingRightChannel;
	}

	public MonoTrack toMonoTrack() {
		MonoTrack monoTrack = new MonoTrack(this.getSampleRate(), this.getBitRate());
		monoTrack.setValues(mixChannelsToMonoChannel());
		return monoTrack;
	}

	@Override
	public StereoTrack toStereoTrack() {
		return this;
	}

	@Override
	public Track clone() {
		// TODO Auto-generated method stub
		return null;
	}

}
