package edgar.oesterlein.audioEditor.audioProcessing;

import java.io.File;
import java.io.IOException;

public interface WaveFile {
	public void saveWaveFile(File file) throws IOException, IllegalArgumentException;		//TODO: IllegalArgumentException durch sinnvollere Alternative erstzen.
	public String toString();
}
