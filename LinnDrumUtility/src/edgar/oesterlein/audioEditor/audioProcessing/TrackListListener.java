package edgar.oesterlein.audioEditor.audioProcessing;

import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.track.Track;

public interface TrackListListener {
	void trackAdded(Track track);
	void trackRemoved(Track track);
}
