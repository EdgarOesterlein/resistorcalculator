package edgar.oesterlein.audioEditor.audioProcessing;

public enum TrackChannelType {
	MONO("Mono"), STEREO("Stereo");
	private final String display;
	
	private TrackChannelType(String display) {
		this.display = display;
	}
	
	@Override
	public String toString() {
		return display;
	}
}
