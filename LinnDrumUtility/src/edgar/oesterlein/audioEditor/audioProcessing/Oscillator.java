package edgar.oesterlein.audioEditor.audioProcessing;

public class Oscillator {
	private double frequency;
	
	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}
	
	public Oscillator() {
		frequency = 440; 	//Kammerton
		//amplification = Math.pow(2, bitsPerSample-1)-1;
	}
	
	public double sine(double time) {
		return Math.sin(time * 2 * Math.PI * frequency) * Integer.MAX_VALUE;
	}
	
	public double square(double time) {
		return sine(time) >= 0 ? (1.0f * Integer.MAX_VALUE) : (-1.0f * Integer.MAX_VALUE);
	}
	
	public double sawtooth(double time) {
		return (2 * (time * frequency - Math.floor(time * frequency + 0.5))) * Integer.MAX_VALUE;
	}
	
	public double triangle(double time) {		//TODO: Geht nicht, wahrscheinlich wegen der amplification.
		return (Math.abs((2 * (time * frequency - Math.floor(time * frequency + 0.5)))) * 2.0f - 1.0f) * Integer.MAX_VALUE;
	}
	
	public double whiteNoise() {
		double rnd = Math.random();
		if(rnd < 0.5)
			return Math.random() * Integer.MIN_VALUE;
		else
			return Math.random() * Integer.MAX_VALUE;
	}
}
