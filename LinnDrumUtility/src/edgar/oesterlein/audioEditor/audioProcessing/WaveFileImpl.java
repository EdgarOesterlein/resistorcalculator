package edgar.oesterlein.audioEditor.audioProcessing;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import edgar.oesterlein.audioEditor.util.KMPMatch;
import edgar.oesterlein.audioEditor.util.PreparationTool;

import org.apache.commons.io.FileUtils;

//TODO: TestCase erstellen und durchf�hren.

/*
 * Currently this class only supports mono and stereo tracks. But I think these are sufficient.
 * The values are stored as 32 bit integers.
 * */

public class WaveFileImpl implements WaveFile{
	private int chunkID, format;
	private int chunkSize;
	private int subChunk1ID;	
	private int subChunk1Size;
	private short audioFormat, numChannels;		//TODO: Support for 2 Channels.
	private int sampleRate, byteRate;
	private short blockAlign, bitsPerSample;
	private int subChunk2ID;
	private int subChunk2Size;
	private ArrayList<Integer> values;	//For mono and stereo
	
	public ArrayList<Integer> getValues() {
		return values;
	}

	public void setValues(ArrayList<Integer> values) {
		this.values = values;
		subChunk2Size = this.values.size() / numChannels * blockAlign;	
		chunkSize = 36 + subChunk2Size;
	}

	//Constructs a wave-file without any values. These have to be filled before saving.
	public WaveFileImpl(int sampleRate, int bitsPerSample, int numChannels) {
		chunkID =  0x52494646;	//'RIFF'
		format = 0x57415645; 	//'WAVE'
	
		subChunk1ID = 0x666d7420;	//'fmt ' 
		subChunk1Size = 16;		//IF PCM!
		audioFormat = 1;		//PCM
		this.numChannels = (short)numChannels;
		this.sampleRate = sampleRate;
		this.bitsPerSample = (short)bitsPerSample;
		blockAlign = (short)(numChannels * ((bitsPerSample + 7) / 8));
		byteRate = sampleRate * blockAlign;	
		subChunk2ID = 0x64617461;	//'data'
	};
	
	//Loads an existing wave-file.
	public WaveFileImpl(File file) throws IOException {
		byte[] riffChunkPattern = {0x52, 0x49, 0x46, 0x46};	//'RIFF'
		byte[] fmtChunkPattern = {0x66, 0x6d, 0x74, 0x20};	//'fmt '
		byte[] dataChunkPattern = {0x64, 0x61, 0x74, 0x61};	//'data'
		
		byte[] data = FileUtils.readFileToByteArray(file);
		DataInputStream dis = new DataInputStream(new ByteArrayInputStream(data));
		
		int n = KMPMatch.indexOf(data, riffChunkPattern);
		dis.skipBytes(n+4);
		chunkSize = Integer.reverseBytes(dis.readInt());
		format = dis.readInt();
		
		System.out.println("ChunkSize: \t" + chunkSize);
		System.out.println("Format: \t0x" + Integer.toHexString(format));
		
		dis.reset();
		n = KMPMatch.indexOf(data, fmtChunkPattern);
		dis.skipBytes(n+4);
		subChunk1Size = Integer.reverseBytes(dis.readInt());
		audioFormat = Short.reverseBytes(dis.readShort());
		numChannels = Short.reverseBytes(dis.readShort());
		sampleRate = Integer.reverseBytes(dis.readInt());
		byteRate = Integer.reverseBytes(dis.readInt());
		blockAlign = Short.reverseBytes(dis.readShort());
		bitsPerSample = Short.reverseBytes(dis.readShort());
		
		System.out.println("SubChunk1Size: \t" + subChunk1Size);
		System.out.println("AudioFormat: \t" + audioFormat);
		System.out.println("NumChannels: \t" + numChannels);
		System.out.println("Samplerate: \t" + sampleRate);
		System.out.println("Byterate: \t" + byteRate);
		System.out.println("Bits per sample: " + bitsPerSample);
		
		dis.reset();
		n = KMPMatch.indexOf(data, dataChunkPattern);
		dis.skipBytes(n+4);
		subChunk2Size = Integer.reverseBytes(dis.readInt());
		
		System.out.println("SubChunk2Size: \t" + subChunk2Size);
		
		//Gilt nur f�r MONO
		if(bitsPerSample == 8) {
			values = new ArrayList<Integer>(subChunk2Size);
			for(int i = 0; i < subChunk2Size; i++) 
				values.add(dis.readUnsignedByte());
		}
		else if(bitsPerSample == 16) {
			int valuesCount = subChunk2Size/2;
			values = new ArrayList<Integer>(subChunk2Size/2);
			for(int i = 0; i < valuesCount; i++)
				values.add((int)Short.reverseBytes(dis.readShort()));
		}
		else if(bitsPerSample == 32) {
			int valuesCount = subChunk2Size/4;
			values = new ArrayList<Integer>(valuesCount);
			for(int i = 0; i < valuesCount; i++)
				values.add(Integer.reverseBytes(dis.readInt()));
		}
		else 
			throw new IllegalArgumentException("Es werden nur 8, 16 und 32 bit unterst�tzt :(");
		
		
		//TODO: DATA EINLESEN!!!
		dis.close();
	}
	
	public static ArrayList<Integer> mergeToStereoTrack(ArrayList<Integer> leftChannelTrack, ArrayList<Integer> rightChannelTrack) {
		if(leftChannelTrack.size() == rightChannelTrack.size()) {
			ArrayList<Integer> mergedTrack = new ArrayList<Integer>(leftChannelTrack.size() + rightChannelTrack.size());
			for(int i = 0; i < leftChannelTrack.size(); i++) {
				mergedTrack.add(leftChannelTrack.get(i));
				mergedTrack.add(rightChannelTrack.get(i));
			}
			return mergedTrack;
		}
		else throw new IllegalArgumentException("Both tracks have not the same size!");
	}
	
	/*
	public void loadWaveFile(File file) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		int value;
		while((value = br.read()) != -1) {
			values.add(value);
		}
		br.close();
	}
	*/
	
	
	
	public void saveWaveFile(File file) throws IOException {
		if(values.size() == 0 || values.equals(null)) {
			throw new IllegalArgumentException("Please fill up the WaveFile with data first.");	
		}
		
		File outputFile = new File(file.getAbsolutePath() + ".wav");
		DataOutputStream dos = new DataOutputStream(new FileOutputStream(outputFile));
		dos.writeInt(chunkID);
		dos.writeInt(Integer.reverseBytes(chunkSize));
		dos.writeInt(format);
		dos.writeInt(subChunk1ID);
		dos.writeInt(Integer.reverseBytes(subChunk1Size));
		dos.writeShort(Short.reverseBytes(audioFormat));
		dos.writeShort(Short.reverseBytes(numChannels));
		dos.writeInt(Integer.reverseBytes(sampleRate));
		dos.writeInt(Integer.reverseBytes(byteRate));
		dos.writeShort(Short.reverseBytes(blockAlign));
		dos.writeShort(Short.reverseBytes(bitsPerSample));
		dos.writeInt(subChunk2ID);
		dos.writeInt(Integer.reverseBytes(subChunk2Size));
		
		//Converting and writing values to output.
		if(bitsPerSample == 8) {
			for(int i = 0; i < values.size(); i++) {
				values.set(i, (int)PreparationTool.map(values.get(i), Integer.MIN_VALUE, Integer.MAX_VALUE, 0, 255));
				dos.writeByte(values.get(i));
			}
		}
		else if(bitsPerSample == 16) {
			for(int i = 0; i < values.size(); i++) {
				//int value = values.get(i);
				int value = (int)PreparationTool.map(values.get(i), 0, 16383, Short.MIN_VALUE, Short.MAX_VALUE);
				short value2 = (short)value;
				value2 = Short.reverseBytes(value2);
				//System.out.println(Integer.toHexString(value));
				//dos.writeShort(Short.reverseBytes((short)value));
				dos.writeShort(value2);
			}
		}
		else if(bitsPerSample == 32) {
			for(int i = 0; i < values.size(); i++) {
				//int value = (int)PreparationTool.map(values.get(i), 0, 255, Integer.MIN_VALUE, Integer.MAX_VALUE);
				int value = values.get(i);
				dos.writeInt(Integer.reverseBytes(value));
				//dos.writeInt(value);
			}
		}
		dos.close();
	}
	
	@Override
	public String toString() {
		return "";
	}
}
