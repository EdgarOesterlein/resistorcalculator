package edgar.oesterlein.audioEditor.audioProcessing;

public class Mixer {
	int volume_channel1 = 0;
	int volume_channel2 = 0;
	
	public Mixer() {
		
	}
	
	//This mixing technique provides hard clipping.
	public int mix(int signal_channel1, int signal_channel2) {
		long result = (long)(signal_channel1 + signal_channel2);
		if(result > Integer.MAX_VALUE)
			result = Integer.MAX_VALUE;
		else if(result < Integer.MIN_VALUE)
			result = Integer.MIN_VALUE;
		return (int)result;
	}
}
