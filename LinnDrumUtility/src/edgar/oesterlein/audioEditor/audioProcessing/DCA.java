package edgar.oesterlein.audioEditor.audioProcessing;

//Digitally Controlled Attenuator
public class DCA {
	public static double process(double audioSignal, double amplitude) {
		return audioSignal * amplitude;
	}
}
