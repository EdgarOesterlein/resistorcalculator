package edgar.oesterlein.audioEditor.command;

import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.model.MainModel;

//TODO: this command class SHOULD NOT BE USED as it is. Some improvements have to be done.

public class AddEmptyMonoTrackCommand extends Command {
	private MainModel mainModel;
	private MonoTrack monoTrack_ref;
	
	public AddEmptyMonoTrackCommand(MainModel mainModel) {
		this.mainModel = mainModel;
	}
	
	//Copy-Constructor
	public AddEmptyMonoTrackCommand(AddEmptyMonoTrackCommand cmd) {
		super(cmd);
		this.mainModel = cmd.mainModel;
		this.monoTrack_ref = cmd.monoTrack_ref;
	}

	@Override
	public void executeCommand() {
		monoTrack_ref = mainModel.addEmptyMonoTrack();
	}

	@Override
	public Command undo() {
		return new RemoveTrackCommand(mainModel, monoTrack_ref);
	}

	@Override
	public Command clone() {
		return new AddEmptyMonoTrackCommand(this);
	}
}
