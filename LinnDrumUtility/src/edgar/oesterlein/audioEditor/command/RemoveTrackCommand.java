package edgar.oesterlein.audioEditor.command;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.model.MainModel;

public class RemoveTrackCommand extends Command {
	
	private MainModel mainModel;
	private Track track;
	private Track trackBackup;
	
	public RemoveTrackCommand(MainModel mainModel, Track track) {
		this.mainModel = mainModel;
		this.track = track;
		createBackup();
	}
	
	//Copy-Constructor
	public RemoveTrackCommand(RemoveTrackCommand cmd) {
		super(cmd);
		this.mainModel = cmd.mainModel;
		this.track = cmd.track;
		this.trackBackup = cmd.trackBackup;
	}
	
	public void createBackup() {
		if(track.getTrackChannelType() == TrackChannelType.MONO)
			trackBackup = new MonoTrack(track.toMonoTrack()); 
		else if(track.getTrackChannelType() == TrackChannelType.STEREO)
			trackBackup = new StereoTrack(track.toStereoTrack());
	}
	
	@Override
	public void executeCommand() {
		mainModel.removeTrack(track);
	}

	@Override
	public Command undo() {
		return new AddTrackCommand(mainModel, trackBackup);
	}

	@Override
	public Command clone() {
		return new RemoveTrackCommand(this);
	}

}
