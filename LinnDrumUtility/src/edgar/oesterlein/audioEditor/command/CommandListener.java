package edgar.oesterlein.audioEditor.command;

import java.util.List;

public interface CommandListener {
	void commandListUpdated(List<Command> commandListCopy);
	void redoCommandListUpdated(List<Command> redoCommandListCopy);
}
