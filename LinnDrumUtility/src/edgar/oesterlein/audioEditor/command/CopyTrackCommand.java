package edgar.oesterlein.audioEditor.command;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.model.MainModel;

public class CopyTrackCommand extends Command {
	private MainModel mainModel;
	private Track createdTrack;
	
	public CopyTrackCommand(MainModel mainModel, Track track) {
		createdTrack = track.clone();
	}
	
	//Copy-Constructor
	public CopyTrackCommand(CopyTrackCommand cmd) {
		super(cmd);
		this.mainModel = cmd.mainModel;
		this.createdTrack = cmd.createdTrack;
	}
	
	@Override
	public void executeCommand() {
		if(createdTrack.getTrackChannelType() == TrackChannelType.MONO)
			mainModel.copyTrack(createdTrack);
	}

	@Override
	public Command undo() {
		return new RemoveTrackCommand(mainModel, createdTrack);
	}

	@Override
	public Command clone() {
		return new CopyTrackCommand(this);
	}

}
