package edgar.oesterlein.audioEditor.command;

import java.util.ArrayList;
import java.util.List;

import edgar.oesterlein.audioEditor.audioProcessing.TrackChannelType;
import edgar.oesterlein.audioEditor.audioProcessing.track.MonoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.StereoTrack;
import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.model.ToneGeneratorModel;

public class ToneGenerationCommand extends Command {
	private ToneGeneratorModel toneGeneratorModel_ref;
	private List<Track> trackBackupList;
	
	public ToneGenerationCommand(ToneGeneratorModel toneGeneratorModel_ref) {
		this.toneGeneratorModel_ref = toneGeneratorModel_ref;
		trackBackupList = new ArrayList<>(toneGeneratorModel_ref.getSelectedTracks().size());
	}
	
	//Copy-Constructor
	public ToneGenerationCommand(ToneGenerationCommand cmd) {
		super(cmd);
		this.toneGeneratorModel_ref = cmd.toneGeneratorModel_ref;
		this.trackBackupList = cmd.trackBackupList;
	}
	
	@Override
	public void executeCommand() {
		createBackup();
		toneGeneratorModel_ref.generate();
	}

	//A copy of the selectedTrack-list will be created.
	private void createBackup() {
		for(Track track : toneGeneratorModel_ref.getSelectedTracks()) {
			if(track.getTrackChannelType() == TrackChannelType.MONO) 
				trackBackupList.add(new MonoTrack(track.toMonoTrack()));
			else if(track.getTrackChannelType() == TrackChannelType.STEREO)
				trackBackupList.add(new StereoTrack(track.toStereoTrack()));
		}
	}

	@Override
	public Command undo() {
		// TODO Auto-generated method stub
		System.out.println("Undo action not implemented yet...");
		return null;
	}

	@Override
	public Command clone() {
		return new ToneGenerationCommand(this);
	}

}
