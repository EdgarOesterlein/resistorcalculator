package edgar.oesterlein.audioEditor.command;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// 																									   //
//  IMPORTANT: Do NOT use these methods directly! In able to undo and redo the actions of the user     //
//  it is important to execute the commands with the CommandManager class. 		                       //
//  The CommandManager contains several methods to, for example, execute, undo and redo your actions.  //
//																									   //
/////////////////////////////////////////////////////////////////////////////////////////////////////////
public abstract class Command implements Cloneable{
	public abstract void executeCommand();	
	public abstract Command undo();
	public abstract Command clone();
	
	private StringBuilder sb;
	
	public Command() {}
	
	public Command(Command cmd) {
		sb = cmd.sb;
	}
	
	//Actually this method is just a preparation for the real execution, which is called at the end.
	public void execute() {
		buildStringRepresentation();
		executeCommand();
	}
	
	private void buildStringRepresentation() {
		if(sb == null) {
			sb = new StringBuilder(getClass().getSimpleName().toString()); //Get the class name
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");	//mark the timestamp
			sb.append(" - ");
			sb.append(sdf.format(Calendar.getInstance().getTime()));
			sb.append(" Uhr");
		}
	}
	
	@Override
	public String toString() {
		return sb.toString();
	}
}
