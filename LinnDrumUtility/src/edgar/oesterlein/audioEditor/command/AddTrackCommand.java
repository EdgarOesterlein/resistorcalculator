package edgar.oesterlein.audioEditor.command;

import edgar.oesterlein.audioEditor.audioProcessing.track.Track;
import edgar.oesterlein.audioEditor.model.MainModel;

public class AddTrackCommand extends Command {
	private MainModel mainModel;
	private Track track;
	
	
	
	public AddTrackCommand(MainModel mainModel, Track track) {
		super();
		this.mainModel = mainModel;
		this.track = track;
	}
	
	//Copy-Constructor
	public AddTrackCommand(AddTrackCommand cmd) {
		super(cmd);
		this.mainModel = cmd.mainModel;
		this.track = cmd.track;
	}

	@Override
	public void executeCommand() {
		mainModel.addTrack(track);
	}

	@Override
	public Command undo() {
		return new RemoveTrackCommand(mainModel, track);
	}

	@Override
	public Command clone() {
		return new AddTrackCommand(this);
	}

}
