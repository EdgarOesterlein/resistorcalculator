package edgar.oesterlein.audioEditor.command;

import java.util.ArrayList;
import java.util.List;

public class CommandManager {
	//SINGLETON MECHANISM//////////////////////////////////
	private static CommandManager instance;
	
	public static CommandManager getInstance() {
		if(instance == null)
			instance = new CommandManager();
		return instance;
	}
	
	private CommandManager() {
		super();
	}
	///////////////////////////////////////////////////////
	private List<CommandListener> commandListenerList = new ArrayList<>(5); 
	
	//Just returns a copy
	public void notifyCmdListUpdated() {
		boolean cmdListenerExistent = commandListenerList != null && commandListenerList.size() > 0;
		boolean cmdListUsable = commandList != null;
		
		if(cmdListenerExistent && cmdListUsable)
			for(CommandListener cl : commandListenerList)
				cl.commandListUpdated(getCommandList());
	}
	
	//Just returns a copy
	public void notifyRedoCmdListUpdated() {
		boolean cmdListenerExistent = commandListenerList != null && commandListenerList.size() > 0;
		boolean cmdListUsable = redoCommandList != null;
		
		if(cmdListenerExistent && cmdListUsable)
			for(CommandListener cl : commandListenerList)
				cl.redoCommandListUpdated(getRedoCommandList());
	}
	
	public void addCommandListener(CommandListener l) {
		commandListenerList.add(l);
	}
	
	public void removeCommandListener(CommandListener l) {
		commandListenerList.remove(l);
	}
	
	
	private final int COMMANDLIST_SIZE_INIT = 100;
	List<Command> commandList = new ArrayList<>(COMMANDLIST_SIZE_INIT);
	List<Command> redoCommandList = new ArrayList<>(COMMANDLIST_SIZE_INIT);
	
	private Command getLastCmdFromCommandList() {
		int size = getCommandListSize();
		if(size > 0) 
			return commandList.get(size-1);
		else
			return null;
	}
	
	private Command getLastCmdFromRedoList() {
		int size = getRedoCommandListSize();
		if(size > 0) 
			return redoCommandList.get(size-1);
		else
			return null;
	}
	
	
	
	public List<Command> getCommandList() {
		return commandList;
	}

	public List<Command> getRedoCommandList() {
		return redoCommandList;
	}

	/*
	A copy of the new command will be added to the commandList. A copy is needed because 
	otherwise everytime the same reference of the command will be added to the commandList. 
	The redoCommandList will be cleared and the new command will be executed.
	*/
	//TODO: Timestamp implementation
	public void executeCommand(Command cmd) {
		Command cmdClone = cmd.clone();
		commandList.add(cmdClone);
		redoCommandList.clear();
		cmdClone.execute();
		
		notifyCmdListUpdated();
		notifyRedoCmdListUpdated();
	}

	public void undo() {
		Command cmd = getLastCmdFromCommandList();
		cmd.undo().execute();
		redoCommandList.add(cmd);
		commandList.remove(cmd);
		
		notifyCmdListUpdated();
		notifyRedoCmdListUpdated();
	}
	
	public void redo() {
		Command cmd = getLastCmdFromRedoList();
		cmd.execute();
		commandList.add(cmd);
		redoCommandList.remove(cmd);
		
		notifyCmdListUpdated();
		notifyRedoCmdListUpdated();
	}
	
	public int getCommandListSize() {
		return commandList.size();
	}
	
	public int getRedoCommandListSize() {
		return redoCommandList.size();
	}
}
